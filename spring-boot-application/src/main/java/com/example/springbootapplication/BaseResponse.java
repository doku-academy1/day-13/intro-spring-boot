package com.example.springbootapplication;

public class BaseResponse {
    private String message, status;
    private Object user;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public Object getUser() {
        return user;
    }

    public BaseResponse(String message, String status, Object user) {
        this.message = message;
        this.status = status;
        this.user = user;
    }
}
