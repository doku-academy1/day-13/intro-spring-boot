package com.example.springbootapplication;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.ArrayList;

@RestController
public class helloWorld {

    @GetMapping("/v1/messages")
    public ResponseEntity<Response> getHelloWorld() {
        User user = new User("Farhan", "farhan@gmail.com");
        Response response = new Response("Hello World", HttpStatus.OK.toString(), user);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("v1/messages")
    public ResponseEntity<Response> postHelloWorld() {
        User user = new User("Farhan", "farhan@gmail.com");
        Response response = new Response("Message 'Hello World' sent!", HttpStatus.CREATED.toString(), user);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("v1/messages")
    public ResponseEntity<Response> deleteHelloWorld() {
        User user = new User("Farhan", "farhan@gmail.com");
        Response response = new Response("Message 'Hello World' deleted!", HttpStatus.ACCEPTED.toString(), user);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }
}
