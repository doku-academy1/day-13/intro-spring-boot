package com.example.springbootapplication;

public class User {
    private String name, email;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }
}
